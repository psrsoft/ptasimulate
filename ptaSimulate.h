#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define MAX_STRLEN 1024
#define MAX_PARAMS 40 // Maximum number of parameters in an ephemeris
#define MAX_PSRS 50 // Maximum number of pulsars
#define MAX_LINE_PARAMS 20 // Maximum number of parameters on a script line
#define MAX_OBSRUN 10 // Maximum number of observation runs
#define MAX_SCHED 10 // Maximum number of schedules
#define MAX_OBS_SCHED 50 // Maximum number of observations in schedule
#define MAX_TOAS 1000 // Maximum number of ToAs per pulsar
#define MAX_TNOISE 50 // Maximum number of timing noise definitions
#define MAX_RCVR 20 // Maximum number of receivers
#define MAX_BE 20 // Maximum number of backends
#define MAX_FLUX 10 // Maximum flux density values per pulsar
#define MAX_TSKY 10 // Maximum tsky values per pulsar
#define MAX_PROF_FILE 10 // Maximum number of profiles per pulsar
#define MAX_DMVAR 50 // Maximum number of DM variations definitions
#define MAX_DMCOVAR 50 // Maximum number of DM covariance definitions
#define MAX_DMFUNC 50 // Maximum number of DM function definitions
#define MAX_JITTER 50 // Maximum number of jitter definitions
#define MAX_SYS 5 // Maximum number of simultaneous systems in a given obsSys
#define MAX_OBS_SYS 10 
#define MAX_FDIST_VALS 1000
#define MAX_T2TIM 50 // Maximum number of tempo2 tim files

#define DM_CONST    2.41e-4

typedef struct paramStruct {
  char l[MAX_STRLEN]; // Label
  char v[MAX_STRLEN]; // Value
  int type; // 1 = just value, 2 = label and value
} paramStruct;

typedef struct valStruct {
  char inVal[MAX_STRLEN];
  double dval;
  int set; // 1 = set dval
  int constant; // 1 = Remain constant for different realisations, 0 = change
} valStruct;


typedef struct t2TimStruct {
  char psrName[MAX_STRLEN];
  int  fileName[MAX_STRLEN];
  int  psrNum;
} t2TimStruct;

typedef struct rcvrStruct {
  char name[MAX_STRLEN];
  valStruct flo;
  valStruct fhi;
  valStruct tsys;
  valStruct gain; // In K/Jy
} rcvrStruct;

typedef struct beStruct {
  char name[MAX_STRLEN];
  valStruct bw;
  valStruct nbin;
} beStruct;

typedef struct obsSysStruct {
  char name[MAX_STRLEN];
  int nSys;
  int rcvrNum[MAX_SYS];
  int beNum[MAX_SYS];
  char rcvrName[MAX_SYS][128];
  char beName[MAX_SYS][128];
  valStruct freq[MAX_SYS];
} obsSysStruct;

typedef struct obsStruct {
  char tel[MAX_STRLEN];
  char sched[MAX_STRLEN];
  char or[MAX_STRLEN];
  int psrNum;
  valStruct start;
  valStruct finish;
  valStruct toaErr;
  valStruct freq;
  valStruct tobs;
  long double sat;
  int satSet;
  int rcvrNum;
  int beNum;
  int obsSysNum;
} obsStruct;

typedef struct psrStruct {
  char name[MAX_STRLEN]; int setName;
  char label[MAX_STRLEN]; int setLabel;
  char ephem[MAX_STRLEN]; int setEphem;
  char rajStr[MAX_STRLEN];
  char decjStr[MAX_STRLEN];
  int  requireCatRead;
  int  requireEphemRead;
  int  nSetParam;  // Number of PSRCAT parameters set
  char setParamName[MAX_PARAMS][MAX_STRLEN];
  valStruct paramVal[MAX_PARAMS];
  int nToAs;
  obsStruct obs[MAX_TOAS];
  double rajd; // Position in degrees
  double decjd; // Position in degrees
  double dm; // dispersion measure
  double dist; // Distance in metres
  double p0; // Period
  double f0; // frequency
  char profileEqn[MAX_STRLEN]; int setProfileEqn;
  char profileFile[MAX_PROF_FILE][MAX_STRLEN]; valStruct freqProfileFile[MAX_PROF_FILE]; 
  int nProfileFile;
  valStruct flux[MAX_FLUX]; valStruct freqFlux[MAX_FLUX]; int nFlux;
  valStruct tsky[MAX_FLUX]; valStruct freqTsky[MAX_FLUX]; int nTsky;
  valStruct diff_df; valStruct diff_dfFreq; int setDiff_df;
  valStruct diff_ts; valStruct diff_tsFreq; int setDiff_ts;
} psrStruct;

typedef struct obsrunStruct {
  char name[MAX_STRLEN];
  char tel[MAX_STRLEN];
  char sched[MAX_STRLEN];
  int  setSched;
  valStruct start;
  valStruct finish;
  valStruct cadence;
  valStruct probFailure;
  t2TimStruct T2Tim[MAX_T2TIM];
  int nT2Tim;
} obsrunStruct;


typedef struct scheduleStruct {
  char name[MAX_STRLEN];
  obsStruct obs[MAX_OBS_SCHED];
  int nObsSched;
} scheduleStruct;

typedef struct gwStruct {
  valStruct amp;
  valStruct alpha;
} gwStruct;

typedef struct tnoiseStruct {
  int psrNum;
  valStruct alpha;
  valStruct p0;
  valStruct fc;
  char predictMode[MAX_STRLEN];
} tnoiseStruct;

typedef struct dmVarStruct {
  int psrNum;
  valStruct d_tscale;
  valStruct dVal;
  valStruct refFreq;
  int type;
} dmVarStruct;

typedef struct dmCovarStruct {
  int psrNum;
  valStruct alpha;
  valStruct a;
  valStruct b;
  int type;
} dmCovarStruct;

typedef struct dmFuncStruct {
  int psrNum;
  valStruct ddm;
} dmFuncStruct;

typedef struct jitterStruct {
  int psrNum;
  valStruct t0;
  valStruct sigma_j;
  valStruct refFreq;
  int type;
} jitterStruct;


typedef struct controlStruct {
  char name[MAX_STRLEN];
  int nproc;

  long seed; // Random number seed
  psrStruct psr[MAX_PSRS];
  int npsr; // Number of pulsars
  char inputScript[MAX_STRLEN];
  int nreal; // Number of realisations

  obsrunStruct obsRun[MAX_OBSRUN];
  int nObsRun; // Number of observation runs

  scheduleStruct sched[MAX_SCHED];
  int nSched; // Number of available schedules

  tnoiseStruct tnoise[MAX_TNOISE];
  int nTnoise;

  dmVarStruct dmVar[MAX_DMVAR];
  int nDMvar;

  dmCovarStruct dmCovar[MAX_DMCOVAR];
  int nDMcovar;

  dmFuncStruct dmFunc[MAX_DMFUNC];
  int nDMfunc;

  jitterStruct jitter[MAX_DMVAR];
  int nJitter;


  long double minT;
  long double maxT;

  gwStruct gw;
  int nGW;

  rcvrStruct rcvr[MAX_RCVR];
  int nRCVR;

  beStruct be[MAX_BE];
  int nBE;

  char simEphem[MAX_STRLEN];
  char simClock[MAX_STRLEN];
  char useEphem[MAX_STRLEN];
  char useClock[MAX_STRLEN];

  obsSysStruct obsSys[MAX_OBS_SYS];
  int nObsSys;


  char t2exe[MAX_STRLEN];

} controlStruct;
